﻿using Client.Command;
using Client.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Windows;
using System.Windows.Input;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Client.ViewModel
{
    class MainViewModel : INotifyPropertyChanged
    {
        public ICommand SendCommand { get; set; }
        public ICommand LoadImageCommand { get; set; }

        public Message Message { get; set; }
        public List<Message> Messages { get; set; }

        public string ImagePath { get; set; }

        public MainViewModel()
        {
            SendCommand = new RelayCommand(Send, CanSend);
            LoadImageCommand = new RelayCommand(LoadImage);

            Message = new Message();

            Messages = new List<Message>();

            // Fetch messages only on application launch:
            FetchMessages().ContinueWith(task =>
            {
                task.Result.ForEach(r =>
                {
                    FetchSingleMessage(r).ContinueWith(message =>
                    {
                        Messages.Add(message.Result);
                    });
                });
            });
        }

        /// <summary>
        /// Opens dialog to load image to binded object.
        /// </summary>
        private void LoadImage(object obj)
        {
            var dlg = new OpenFileDialog();

            dlg.DefaultExt = ".png";
            dlg.Filter = "Image files (.png)|*.png|*.jpg|*.jpeg";
            dlg.Multiselect = false;

            Nullable<bool> dialogResult = dlg.ShowDialog();

            if (dialogResult != true)
            {
                MessageBox.Show("You need to select image to open");
                return;
            }
            
            this.ImagePath = dlg.FileName;
            this.Message.Image = File.ReadAllBytes(dlg.FileName);
        }

        /// <summary>
        /// Check if message image is not null aswell as message text.
        /// </summary>
        /// <param name="obj">Parameter passed by view</param>
        /// <returns>True if message is sendable</returns>
        private bool CanSend(object obj)
        {
            if (string.IsNullOrEmpty(this.Message.Text) || this.Message.Image == null)
                return false;

            return true;
        }

        /// <summary>
        /// Sends <see cref="Model.Message" /> binded object to server.
        /// </summary>
        /// <param name="obj">Parameter passed by view</param>
        private async void Send(object obj)
        {
            try
            {
                string url = "https://localhost:7012/api/Message/InsertMessage";

                var postContent = new Dictionary<string, string>()
                {
                    { "text", this.Message.Text},
                    { "image", Convert.ToBase64String(this.Message.Image)}
                };

                using (HttpClient client = new HttpClient())
                {
                    var content = new FormUrlEncodedContent(postContent);
                    var response = await client.PostAsync(url, content);

                    Trace.WriteLine(response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Fetch single message from web api server
        /// </summary>
        /// <param name="id">Server image guid</param>
        /// <returns><see cref="Model.Message"/></returns>
        private async Task<Message> FetchSingleMessage(string id)
        {
            Message message = new Message();

            try
            {
                string url = "https://localhost:7012/api/Message/GetMessage?id=";

                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(url + id);
                    var responseString = await response.Content.ReadAsStringAsync();

                    var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseString);
                    string? date = result?.GetValueOrDefault("createdAt");
                    if (date != null)
                    {
                        message.Date = DateTime.Parse(date);
                    }
                    message.Image = Convert.FromBase64String(result?.GetValueOrDefault("image"));
                    message.Text = result?.GetValueOrDefault("text");
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine($"Server fetch failed: {ex.Message}");
            }

            return message;
        }

        /// <summary>
        /// Fetch server messages
        /// </summary>
        /// <returns>Guid of all messages as List</returns>
        private async Task<List<string>?> FetchMessages()
        {
            List<string>? messageIds = new List<string>();

            try
            {
                string url = "https://localhost:7012/api/Message/GetAllMessages";

                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(url);
                    var responseString = await response.Content.ReadAsStringAsync();

                    string alldicomfile = responseString;
                    messageIds = JsonConvert.DeserializeObject<List<string>>(responseString);
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine($"Server fetch failed: {ex.Message}");
            }

            return messageIds;
        }

        public event PropertyChangedEventHandler? PropertyChanged;
    }
}
