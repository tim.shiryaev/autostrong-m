﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace Client.Command
{
    internal class RelayCommand : ICommand
    {
        private Action<object> _Execute { get; set; }
        private Predicate<object>? _CanExecute { get; set; }

        public RelayCommand(Action<object> execute) : this(execute, null) { }
        public RelayCommand(Action<object> execute, Predicate<object>? canExecute)
        {
            if (execute == null) 
                throw new ArgumentNullException(nameof(execute));

            _Execute = execute;
            _CanExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            if (_CanExecute != null)
                return _CanExecute(parameter);

            return true;
        }

        public event EventHandler? CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            _Execute(parameter);
        }
    }
}
