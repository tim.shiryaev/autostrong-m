﻿using System;
using System.ComponentModel;

namespace Client.Model
{
    internal class Message : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        public string? Text { get; set; }
        public byte[]? Image { get; set; }
        public DateTime Date { get; set; }
    }
}
